import { apiStatus } from '../../../lib/util';
import { Router } from 'express';

const Magento2Client = require('magento2-rest-client').Magento2Client;

module.exports = ({ config, db }) => {
  let mcApi = Router();

  mcApi.post('/contact', (req, res) => {
    let contactData = req.body
    if (!contactData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('contact', (restClient) => {
      var module = {};
      module.contact = function () {
        return restClient.post('/contact', contactData)
      }
      return module;
    })

    client.contact.contact().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  return mcApi
}
